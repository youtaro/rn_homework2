/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import AppHeader from './components/AppHeader';
import Post from './components/Post';
import Story from './components/Story';
import VideoChat from './components/VideoChat';
import NewFeeds from './components/NewFeeds';

const App = () => {
  const {myText, container} = styles;
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView>
          <AppHeader />
          <VideoChat />
          <Story />
          <NewFeeds />
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({});

export default App;
