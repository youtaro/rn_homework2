import React, {useState, useRef} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  Pressable,
  TouchableOpacity,
  Alert,
  TextInput,
  Modal,
  TouchableHighlight,
  Dimensions,
  KeyboardAvoidingView,
} from 'react-native';
import {Button, Input} from 'react-native-elements';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/Fontisto';

const AppHeader = () => {
  const {txtIcon, btnAction} = styles;
  const [modalVisible, setModalVisible] = useState(false);
  const ref_input1 = useRef();
  return (
    <SafeAreaView>
      <View
        style={{
          width: '100%',
          height: 58,
          paddingLeft: 11,
          paddingRight: 11,
          alignItems: 'center',
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text style={txtIcon}>facebook</Text>
        <KeyboardAvoidingView>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                flex: 1,
              }}>
              <View
                style={{
                  width: Dimensions.get('screen').width / 1.3,
                  borderRadius: 10,
                  height: 150,
                  backgroundColor: '#fff',
                  borderWidth: 1,
                }}>
                <Input
                  placeholder="username"
                  autoFocus={true}
                  returnKeyType="go"
                  onSubmitEditing={() => ref_input1.current.focus()}
                />
                <Input placeholder="Description" ref={ref_input1} />
              </View>
            </View>
          </Modal>
        </KeyboardAvoidingView>
        <View style={{flexDirection: 'row', marginTop: 3}}>
          <TouchableOpacity>
            <Button
              buttonStyle={{backgroundColor: '#f2f2f0'}}
              icon={<IconAntDesign name="search1" size={30} />}
              containerStyle={{
                borderColor: '#fff',
                borderWidth: 1,
                borderRadius: 50,
                marginRight: 2,
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Button
              buttonStyle={{backgroundColor: '#f2f2f0'}}
              icon={<Icon name="messenger" size={30} />}
              containerStyle={{
                borderColor: '#fff',
                borderWidth: 1,
                borderRadius: 50,
              }}
            />
          </TouchableOpacity>
        </View>
      </View>

      <View style={{marginTop: 15, flexDirection: 'row'}}>
        <Image
          source={{
            uri:
              'https://scontent.fpnh20-1.fna.fbcdn.net/v/t1.0-9/120033411_336451684357184_5831256707254770130_n.jpg?_nc_cat=104&_nc_sid=174925&_nc_eui2=AeGxERAQUR8PIaBbYh3vZkkjrzeMe35lbI-vN4x7fmVsj_R12Q6_Rtu_W5uS43ggXRvKDDUSXJ2pAAS67uWqBg5j&_nc_ohc=OQlpCpIwviYAX_a1gBf&_nc_ht=scontent.fpnh20-1.fna&oh=ef825441f8cd6934f2c087b391409895&oe=5FB1A270',
          }}
          style={{
            marginLeft: 15,
            height: 45,
            width: 45,
            borderRadius: 50,
            borderColor: '#faf8f7',
            borderWidth: 1,
          }}
        />

        <TouchableOpacity
          onPress={() => setModalVisible(true)}
          type="outline"
          style={{
            marginTop: 4,
            height: 40,
            marginLeft: '4%',
            borderRadius: 20,
            borderColor: '#8f8d89',
            borderWidth: 1,
            alignItems: 'center',
            justifyContent: 'center',
            width: '80%',
          }}>
          <Text
            style={{
              fontSize: 15,
              color: '#8f8d89',
            }}>
            What's on your mind?
          </Text>
        </TouchableOpacity>
      </View>
      <View
        style={{
          marginTop: 10,
          borderTopWidth: 0.3,
          borderTopColor: '#bfc7c0',
          flexDirection: 'row',
        }}>
        <View style={btnAction}>
          <IconAntDesign name="videocamera" size={20} color="red" />
          <Text> Live</Text>
        </View>
        <View style={btnAction}>
          <Icon name="photograph" color="green" size={20} />
          <Text> Photo</Text>
        </View>
        <View style={btnAction}>
          <IconAntDesign name="plussquareo" size={20} color="red" />
          <Text> Room</Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  txtIcon: {
    color: '#156be9',
    fontSize: 35,
    marginLeft: 8,
    fontFamily: 'Anton-Regular',
  },
  btnAction: {
    marginTop: 4,
    width: '33%',
    borderRightWidth: 0.3,
    borderRightColor: '#f5f3f2',
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 9,
  },

  // container: {
  //   flex: 1,
  //   alignItems: 'center',
  //   justifyContent: 'center',
  // },
});

export default AppHeader;
