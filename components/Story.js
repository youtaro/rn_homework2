import React from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import {Button} from 'react-native-elements';
import {story} from '../Data/Story';

const Story = () => {
  const {container} = styles;
  return (
    <View>
      <View style={container}>
        <View
          style={{
            width: '27%',
            height: 170,
            backgroundColor: '#f7f8fa',
            marginLeft: '1%',
            borderRadius: 8,
            borderColor: '#f5f5f5',
            borderWidth: 1,
          }}>
          <Image
            style={{
              height: 120,
              borderTopLeftRadius: 8,
              borderTopRightRadius: 8,
            }}
            source={{
              uri:
                'https://scontent.fpnh20-1.fna.fbcdn.net/v/t1.0-9/120033411_336451684357184_5831256707254770130_n.jpg?_nc_cat=104&_nc_sid=174925&_nc_eui2=AeGxERAQUR8PIaBbYh3vZkkjrzeMe35lbI-vN4x7fmVsj_R12Q6_Rtu_W5uS43ggXRvKDDUSXJ2pAAS67uWqBg5j&_nc_ohc=OQlpCpIwviYAX_a1gBf&_nc_ht=scontent.fpnh20-1.fna&oh=ef825441f8cd6934f2c087b391409895&oe=5FB1A270',
            }}
          />
          <Button
            buttonStyle={{borderRadius: 50}}
            icon={<IconAntDesign name="plus" size={20} color="#ffffff" />}
            containerStyle={{
              position: 'absolute',
              top: '60%',
              right: '32%',
              borderColor: '#fff',
              borderWidth: 3,
            }}
          />
          <Text
            style={{
              textAlign: 'center',
              fontSize: 14,
              marginTop: '25%',
            }}>
            Create a Story
          </Text>
        </View>

        <ScrollView
          style={{paddingLeft: 11}}
          showsHorizontalScrollIndicator={false}
          horizontal>
          {story.map((datas, i) => {
            return (
              <View
                key={i}
                style={{
                  width: 106,
                  height: 170,
                  position: 'relative',
                  marginRight: 8,
                }}>
                {/* card  */}
                <Image
                  style={{width: '100%', height: 170, borderRadius: 8}}
                  source={{
                    uri: datas.img,
                  }}
                />

                <View style={{position: 'absolute'}}>
                  <Image
                    source={{
                      uri: datas.img,
                    }}
                    style={{
                      top: 4,
                      left: 5,
                      height: 45,
                      width: 45,
                      borderRadius: 50,
                      borderColor: '#1777f2',
                      borderWidth: 3,
                    }}
                  />
                </View>

                <View
                  style={{
                    width: '100%',
                    position: 'absolute',
                    bottom: 5,
                    left: 9,
                  }}>
                  <Text style={{color: '#fff'}}>{datas.name}</Text>
                </View>
              </View>
            );
          })}
        </ScrollView>
      </View>
      <View
        style={{
          backgroundColor: '#cbcbd2',
          height: 9,
        }}
      />
    </View>
  );
};

export default Story;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 192,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
