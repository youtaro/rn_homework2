import React from 'react';
import {View, Text, Dimensions, TouchableOpacity, Alert} from 'react-native';
import {Card, Image, Icon, Avatar} from 'react-native-elements';

const Post = () => {
  return (
    <>
      <View style={{flex: 1}}>
        <View
          style={{
            height: 50,
            alignItems: 'center',
            justifyContent: 'space-between',
            flexDirection: 'row',
            marginTop: '3%',
            paddingHorizontal: '2%',
          }}>
          <View
            style={{
              // height: Dimensions.get('screen').width,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Avatar
              size={50}
              rounded
              source={{
                uri:
                  'https://cdn.jpegmini.com/user/images/slider_puffin_before_mobile.jpg',
              }}
            />
            <View style={{padding: 10}}>
              <Text style={{color: '#222121', fontSize: 15, fontWeight: '500'}}>
                You Taro
              </Text>
              <View style={{alignItems: 'center', flexDirection: 'row'}}>
                <Text style={{fontSize: 9, color: '#747476'}}>9m {'  '}</Text>
                <Icon size={13} color="#747476" type="entypo" name="globe" />
              </View>
            </View>
          </View>
          <TouchableOpacity
            onPress={() =>
              Alert.alert(
                'ប្រញាប់​ឡើង AOG ​ហ្គេម​ថ្មី កំពុង​ល្បី ​មាន​ការ​ប្រកួត​រាល់​សប្ដាហ៍​ជាមួយ​រង្វាន់​១០​លាន​រៀល​',
              )
            }>
            <Icon
              type="entypo"
              name="dots-three-horizontal"
              size={15}
              color="#222121"
            />
          </TouchableOpacity>
        </View>

        {/* END HEADEE */}
        <Text
          numberOfLines={2}
          style={{fontSize: 12, paddingHorizontal: 11, marginTop: 12}}>
          ប្រញាប់​ឡើង AOG ​ហ្គេម​ថ្មី កំពុង​ល្បី ​មាន​ការ​ប្រកួត​រាល់​សប្ដាហ៍
          ​ជាមួយ​រង្វាន់​១០​លាន​រៀល​
        </Text>
        <Image
          style={{height: Dimensions.get('screen').width}}
          source={{
            uri:
              'https://cdn.sabay.com/cdn/media.sabay.com/media/Football(32)/Football10/5f8e5ac976e28_1603164840_large.jpg',
          }}
        />
      </View>
    </>
  );
};

export default Post;
