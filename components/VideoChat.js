import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
} from 'react-native';
import {userData} from '../Data/User';
import {Badge, Button} from 'react-native-elements';
import IconAntDesign from 'react-native-vector-icons/AntDesign';

const VideoChat = () => {
  return (
    <>
      <View
        style={{
          backgroundColor: '#cbcbd2',
          height: 9,
          marginTop: 2,
        }}
      />
      <View
        style={{
          flexDirection: 'row',
          height: 60,
          width: '100%',
        }}>
        <TouchableOpacity
          type="outline"
          style={{
            marginTop: 10,
            height: 40,
            marginLeft: 3,
            borderRadius: 20,
            borderColor: '#82b1ff',
            borderWidth: 1,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'center',
            width: '30%',
          }}>
          <IconAntDesign name="videocamera" size={20} color="#82b1ff" />
          <Text> Create Room</Text>
        </TouchableOpacity>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          style={{marginTop: 8, height: 48, width: '43%'}}
          horizontal>
          {userData.map((data, i) => {
            return (
              <View>
                <Image
                  key={i}
                  source={{
                    uri: data.img,
                  }}
                  style={{
                    marginLeft: 15,
                    height: 45,
                    width: 45,
                    borderRadius: 50,
                    borderColor: '#faf8f7',
                    borderWidth: 1,
                  }}
                />
                <Badge
                  status={data.status === 't' ? 'success' : ''}
                  badgeStyle={
                    data.status === 't'
                      ? {height: 12, width: 12, borderRadius: 10}
                      : {borderWidth: 0}
                  }
                  containerStyle={{position: 'absolute', top: 2, right: 1}}
                />
              </View>
            );
          })}
        </ScrollView>
      </View>

      <View
        style={{
          backgroundColor: '#cbcbd2',
          height: 9,
          marginTop: 2,
        }}
      />
    </>
  );
};

export default VideoChat;

const styles = StyleSheet.create({});
