import React, {useState} from 'react';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Alert,
  StyleSheet,
} from 'react-native';
import {Card, Image, Icon, Avatar, Divider, Badge} from 'react-native-elements';
import {newFeed} from '../Data/NewFeed';

const NewFeeds = () => {
  const {count, wrapper, row, actionBtn, interactCount, name} = styles;

  const [like, setLike] = useState('black');

  return (
    <>
      <View style={{flex: 1}}>
        {newFeed.map((data, i) => {
          return (
            <View>
              <View style={wrapper}>
                <View style={row}>
                  <Avatar
                    size={50}
                    rounded
                    source={{
                      uri: data.profile,
                    }}
                  />
                  <View style={{padding: 10}}>
                    <Text style={name}>{data.name}</Text>
                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                      <Text style={{fontSize: 9, color: '#747476'}}>
                        9m {'  '}
                      </Text>
                      <Icon
                        size={13}
                        color="#747476"
                        type="entypo"
                        name="globe"
                      />
                    </View>
                  </View>
                </View>
                <TouchableOpacity onPress={() => Alert.alert(data.title)}>
                  <Icon
                    type="entypo"
                    name="dots-three-horizontal"
                    size={15}
                    color="#222121"
                  />
                </TouchableOpacity>
              </View>

              <Text
                numberOfLines={2}
                style={{fontSize: 12, paddingHorizontal: 11, marginTop: 12}}>
                {data.title}
              </Text>
              <Image
                style={{
                  height: Dimensions.get('screen').width,
                  marginTop: '2%',
                }}
                source={{
                  uri: data.img,
                }}
              />

              <View style={{paddingHorizontal: 11}}>
                <View style={interactCount}>
                  <View style={row}>
                    <Text style={count}>{data.like} likes</Text>
                  </View>
                  <View style={row}>
                    <Text style={count}>10 Comments</Text>
                  </View>
                  <Text style={count}>{data.share} shares</Text>
                </View>

                <Divider style={{height: 1, backgroundColor: '#e6e9ed'}} />

                <View style={actionBtn}>
                  <View style={row}>
                    <TouchableOpacity onPress={() => setLike('blue')}>
                      <Icon
                        type="antdesign"
                        name="like1"
                        color={like}
                        size={20}
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={row}>
                    <Icon type="fontawesome" name="comment" size={20} />
                  </View>
                  <TouchableOpacity>
                    <Icon type="fontisto" name="share-a" size={20} />
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={{
                  backgroundColor: '#cbcbd2',
                  height: 9,
                }}
              />
            </View>
          );
        })}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  count: {
    fontSize: 12,
    color: '#565657',
  },
  wrapper: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: '3%',
    paddingHorizontal: '2%',
  },
  row: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  actionBtn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 9,
  },
  interactCount: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 9,
  },
  name: {
    color: '#222121',
    fontSize: 15,
    fontWeight: '500',
  },
});

export default NewFeeds;
